/// This file checks if a Message file is present, if not
// it creates one
// c. Scott Hasserd 2020 v1.0

#include <fstream>
#include <string>
#include <sys/stat.h>
#include <iostream>

using namespace std;

int makeMsgFile() {

	string Messages = "Messages";
	ifstream ifile(Messages);
	if(ifile) {
	
		cout << ("Message Dir check good! ") << endl;

	}else {
		int stat;
		string fileName;
		fileName = "Messages";

		cout << (" Creating Messages directory to store messages... \n") << endl;

		stat = mkdir (fileName.c_str(),0777);

		cout << stat;

	}	

return 0;

}
