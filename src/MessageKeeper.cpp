/// This is a message keeper program to write, save, encrypt, and decrypt messages
//it does this by creating a Messages file, taking in a message name, making a .txt file with that name,
//and encrypting and decrypting upon write and read respectively
// c. Scott Hasserd 2020 v. 1.1
#include<iostream>
#include<fstream>
#include<cctype>
#include<iomanip>
#include "Message.hh"
#include "makeDirIf.hh"
#include "loginManager.hh"

 void create_message();
 void read_message();
 void change_message();
 void MessageKeeper();

int main() {

	makeMsgFile();
	
	loginManager verify;

	int newUser;
	string nUser;
	string nPass;

	cout << "Press 1 to login or 2 to create new user: " << endl;
	cin >> newUser;

	if(newUser == 1) {

        if(verify.login() != false) {
            MessageKeeper();
            
        }
        
    } else if(newUser == 2) {
	
		cout << "Type new user name: \n";
		cin >> nUser;

		cout << "Type new password: \n";
		cin >> nPass;

		verify.addUser(nUser,nPass);
        main();
	} else { 
        
        cout << "Please enter 1 or 2 \n";
        main();
        }
        		return 0;
}
      
	//if(verify.login() != false) {
	void MessageKeeper() {
        int choice;
	
		do {
			cout << ("Welcome to Message Keeper \n With this you can write, save, and retrieve an encrypted message ") << endl;
			cout << (" 1. New Message") << endl;
			cout << (" 2. Read Message") << endl;
			cout << (" 3. Edit Message") << endl;
			cout << (" 0. End & Exit") << endl;

			cin >> choice;
		
			switch(choice) {
		
				case 1: 
				       	create_message();
					break;
				case 2:
					read_message();
					break;
				case 3:
					change_message();
					break;	
				default : cout << "\a";
			}

	
		}  while(choice != 0);

	}
	
void create_message() {
	
	Message createMessage;

	createMessage.nameMsg();
	createMessage.writeMsg();

}

void read_message() {

	Message read;
    
    read.readMsg();
    
}

void change_message() {

	Message changeMessage;

	changeMessage.changeMsg();

}
