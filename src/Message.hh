/// This is the Message class, which forms messages from input, stores, and encrypts them to a file
// c. Scott Hasserd 2020 v. 1.1

#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include "loginManager.hh"
using namespace std;



class Message {

	public: 	

		string msgName;
		string dirName = "Messages/";

		string nameMsg() {
		
			cout << ("Please enter message name, it well be saved as <MessageName>.txt: \n");

			cin >> msgName;

			void writeMsg();
			
			return msgName;

		}

		void writeMsg() {
            loginManager message;
			string body;
	
			cout << ("Please enter message, do NOT press Enter/Return until you are done: \n");
		
			cin.ignore();
			getline(cin,body);
    
            message.saveMessage(body, dirName + msgName + ".txt");
    
		}

		string getName() {

			cout << ("Enter name of Message to read, omit the .txt extension: \n");

			cin >> msgName;
            cout << endl;
	
			return msgName;

		}	

		void readMsg() {
            
            loginManager read;
            
            getName();
    
            cout << read.getMessage(dirName + msgName + ".txt") << endl;
            cout << endl;
	}

		static void changeMsg() {
		
			Message changeBody;
			string answer;

			cout << ("The message currently reads: \n");

			changeBody.readMsg();

			cout << ("Do you want to change it? Y/N: ");

			cin >> answer;

			if (answer == "y" || answer == "Y") {
			
				changeBody.writeMsg();
			
			} else if (answer == "n" || answer == "N" ) {
			
				cout << (" \n");	
			
			} else {
			
				cout << ("Please inupt either the letter 'y' for yes or 'n' for no: ");
			
			}

		}

};
