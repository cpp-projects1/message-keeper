///this is a login manager with
//remove with two (getMessage and saveMessage) program specific addons
//feel free to remove them before use as a login manager
//c. Scott Hasserd 2020 v. 1.1


#pragma once

#include <fstream>
#include <iostream>
#include <math.h>
#include <sstream>

using namespace std;

class loginManager {

	public:

		loginManager() {
		
		}
		
		bool login() {
            string usernameAttempt;
            string passwordAttempt;
            bool accessGranted = false;
		
			cout << ("Enter username and password to access MessageKeeper. \nUsername: ");
			cin >> usernameAttempt;

			int usrID = checkFile(usernameAttempt, "users.dat");
			if (usrID != 0) {
			
				cout << "Password: ";

				cin >> passwordAttempt;
				
				int pwdID = checkFile(passwordAttempt, "pass.dat");
				if (usrID == pwdID) {
				
					return !accessGranted;

				} else {
				
					cout << "Incorrect attempt, please try again: " << endl;
					login();
                    return accessGranted;
				
				}
			} else { 
			
				cout << "Incorrect attempt, please try again: " << endl;
				login();
                return accessGranted;

			} 

				return accessGranted;
		
		}

		  void addUser(const string username, const string password) {

            if(checkFile(username, "users.dat") != 0) {
                
                cout << "That username is not availble." << endl;
                return;
            }

            int id = 1 + getLastId();
            saveFile(username, "users.dat", id);
            saveFile(password, "pass.dat", id);
    }

		int getLastId() {
		
			fstream file;
			file.open("users.dat", ios::in);
			file.seekg(0, ios::end);
			
			if(file.tellg() == -1)
				return 0;

			string s;

			for(int i = -1; s.find("#") == string::npos; i--) {
			
				file.seekg(i, ios::end);
				file >> s;

			}

				file.close();
				s.erase(0, 4);

				int id;

				istringstream(s) >> id;

				return id;
			
		}

		int checkFile(string attempt, const char* p_file) {
		
			string line;
			fstream file;
            
			long long eMessage;
			string currentChar;

			file.open(p_file, ios::in);
			while(1) {
		
				file >> currentChar;	
				if (currentChar.find("#ID:") != string::npos) {
				
					if(attempt == line) {
					
						file.close();
						currentChar.erase(0, 4);
						int id;
						istringstream(currentChar) >> id;
						return id;
					
					} else {
					
						line.erase(line.begin(), line.end());
					
					}
				} else {
				
					istringstream(currentChar) >> eMessage;
					line += (char)decrypt(eMessage);
					currentChar = "";

				}

				if(file.peek() == EOF) {
				
					file.close();
					return 0;
				}
			}

		}

		void saveFile(string p_line, const char* p_fileName, const int& id) {
		
			fstream file;
			file.open(p_fileName, ios::app);
			file.seekg(0, ios::end);

			if(file.tellg() != 0)
				file << "\n";

			file.seekg(0, ios::beg);

			for(int i = 0; i < p_line.length(); i++) {
			
				file << encrypt(p_line[i]);
				file << "\n";
			
			}
		
			file << "#ID:" << id;
			file.close();
		}

		long long encrypt(int p_letter) {
		
			return (powf(p_letter, 7) - 67) / 3499; 
		
		}

		int decrypt(long long p_letter) {
		
			return powf((p_letter * 3499 + 67 ), 1/7.f);
		
		}
		
        void saveMessage(string p_line, string p_fileName) {
            fstream file;
            file.open(p_fileName, ios::out);

            for(int i = 0; i < p_line.length(); i++) {
                file << encrypt(p_line[i]);
                file << "\n";
            }

            file << "0";
            file.close();
    }
		
		 string getMessage(string p_fileName)
    {
        string line;
        fstream file;

        long long eMessage;

        file.open(p_fileName, ios::in);
        
        while(1)
        {
            
            file >> eMessage;
            if(eMessage == 0)
            {
                file.close();
                return line;
            }
            line += (char)decrypt(eMessage);
            }
        }

	private:
        
		string usernameAttempt;
		string passwordAttempt;
};
